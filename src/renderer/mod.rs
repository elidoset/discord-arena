use femtovg::{
    renderer::OpenGl as OGL, Canvas, Color, ImageFlags, Paint, PixelFormat, RenderTarget,
};

use glutin::event_loop::EventLoop;
use glutin::ContextBuilder;

use image::codecs::jpeg::JpegEncoder;
use image::ColorType;
use image::RgbaImage;

const RENDER_PATH: &str = "./render.jpg";

pub async fn render_game() {
    let el = EventLoop::new();
    let context = ContextBuilder::new()
        .build_headless(&el, glutin::dpi::PhysicalSize::new(500, 500))
        .expect("NIK");
    let context = unsafe { context.make_current().expect("NIK 2") };
    let renderer =
        OGL::new(|s| context.get_proc_address(s) as *const _).expect("Cannot create renderer");
    let mut canvas = Canvas::new(renderer).expect("Canvas creation error");
    canvas.set_size(500, 500, 1.0);
    let image = canvas
        .create_image_empty(500, 500, PixelFormat::Rgb8, ImageFlags::NEAREST)
        .expect("NIK");
    canvas.set_render_target(RenderTarget::Image(image));
    canvas
        .add_font("./assets/Roboto-Regular.ttf")
        .expect("could not load font");
    canvas.clear_rect(0, 0, 500, 500, Color::rgb(100, 200, 120));
    canvas.clear_rect(0, 0, 250, 250, Color::rgb(100, 100, 20));
    canvas
        .fill_text(
            2.0,
            20.0,
            "coucou",
            Paint::color(Color::rgba(200, 200, 200, 50)),
        )
        .expect("could not fill");
    let screen = canvas.screenshot().expect("screenshot failed");
    let mut pixs = screen.pixels();
    let save_image = RgbaImage::from_fn(500, 500, |_x, _y| {
        let p = pixs.next().expect("Error");
        image::Rgba::from([p.r, p.g, p.b, p.a])
    });
    let mut out_file = std::fs::File::create(RENDER_PATH).expect("Could not create image file");
    let mut decoder = JpegEncoder::new(&mut out_file);
    decoder
        .encode(&save_image, 500, 500, ColorType::Rgba8)
        .expect("Nik");
}
