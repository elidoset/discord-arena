use super::entity::Entity;
use super::terrain::{Terrain, Unit};
use crate::utils::coords::Position;

pub struct Factory {
    position: Position,
}

pub enum StructureKind {
    Factory(Factory),
}

pub trait Structure {
    fn get_position(&self) -> Position;
}

impl Structure for Factory {
    fn get_position(&self) -> Position {
        return self.position;
    }
}

impl Factory {
    pub fn spawn_entity<'a>(
        &mut self,
        self_unit: &Unit,
        ter: &'a mut Terrain,
        entity: Entity,
    ) -> Result<&'a Unit, ()> {
        ter.add_entity(entity, self.position + Position::new(0, 1))
    }
}
