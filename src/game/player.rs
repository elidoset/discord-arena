use super::entity::{Entity, EntityId, Stats};
use crate::utils::{coords::Position, Turn};

use super::terrain::Unit;
pub type UserId = u32;

pub struct Player {
    pub name: String,
    pub id: UserId,
    pub points: u32,
    next_id: EntityId,
}

use super::terrain::Terrain;
use std::fmt;

pub enum AddPlayerError {
    NotEnoughPoints,
}

impl fmt::Display for AddPlayerError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error adding player : {}", self.to_string())
    }
}

impl fmt::Debug for AddPlayerError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "AddPlayerError: {}", self.to_string())
    }
}

impl std::error::Error for AddPlayerError {}

impl Player {
    pub fn new(id: UserId) -> Player {
        Player {
            name: "coucou".to_string(),
            id: id,
            points: 0,
            next_id: 1,
        }
    }

    pub fn add_entity<'a>(
        &mut self,
        terrain: &'a mut Terrain,
        stats: Stats,
        turn: Turn,
    ) -> Result<&'a Unit, AddPlayerError> {
        let total = stats.total();
        if total > self.points {
            return Result::Err(AddPlayerError::NotEnoughPoints);
        }

        let entity = terrain
            .add_entity(
                Entity::new(self.next_id, stats, self.id, turn),
                Position::new(10, 10),
            )
            .unwrap();
        self.points -= total;
        return Result::Ok(entity);
    }
}

impl PartialEq for Player {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}
