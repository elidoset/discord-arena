use super::entity::Entity;
use super::player::UserId;
use crate::utils::coords::Position;

pub struct Terrain {
    pub units: Vec<Unit>,
}

pub struct Unit {
    pub entity: Entity,
    pub position: Position,
}

impl Terrain {
    pub fn new() -> Terrain {
        Terrain {
            units: Vec::<Unit>::new(),
        }
    }

    pub fn add_entity(&mut self, entity: Entity, position: Position) -> Result<&Unit, ()> {
        self.units.push(Unit {
            entity: entity,
            position: position,
        });
        Result::Ok(&self.units.last().unwrap())
    }
}
