use super::player::UserId;
use super::terrain;
use crate::utils::{coords::Position, Turn};

pub type EntityId = u32;

pub struct Stats {
    pub attack: u32,
    pub life: u32,
}

pub struct Entity {
    pub id: EntityId,
    pub user_id: UserId,
    pub stats: Stats,
    pub creation: Turn,
    pub position: Option<Position>,
}

impl Stats {
    pub fn total(&self) -> u32 {
        self.life + self.attack
    }
}

impl Entity {
    pub fn new(id: EntityId, stats: Stats, user_id: UserId, turn: Turn) -> Entity {
        Entity {
            stats: stats,
            id: id,
            user_id: user_id,
            creation: turn,
            position: None,
        }
    }

    pub fn is_dead(&self) -> bool {
        self.stats.life > 0
    }
}
