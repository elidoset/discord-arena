pub mod entity;
pub mod player;
pub mod structure;
pub mod terrain;

use std::fmt;

pub struct Game {
    pub terrain: terrain::Terrain,
    pub players: Vec<player::Player>,
}

pub enum AddPlayerError {
    PlayerAlreadyPresent,
}

impl fmt::Display for AddPlayerError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "AddPlayerError: {}", self.to_string())
    }
}

impl fmt::Debug for AddPlayerError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "AddPlayerError : {}", self.to_string())
    }
}

impl std::error::Error for AddPlayerError {}
impl Game {
    pub fn create() -> Game {
        Game {
            terrain: terrain::Terrain::new(),
            players: Vec::<player::Player>::new(),
        }
    }

    pub fn add_player(
        &mut self,
        player: player::Player,
    ) -> Result<&player::Player, AddPlayerError> {
        if !self.players.contains(&player) {
            self.players.push(player);
            return Result::Ok(&self.players.last().unwrap());
        } else {
            return Result::Err(AddPlayerError::PlayerAlreadyPresent);
        }
    }

    pub fn debug_init(&mut self) {
        println!("Initializing game");
        self.add_player(player::Player::new(20)).unwrap();
        let p = self.players.iter_mut().find(|i| i.id == 20).unwrap();
        p.points = 42;
        p.add_entity(
            &mut self.terrain,
            entity::Stats {
                life: 5,
                attack: 10,
            },
            1,
        )
        .unwrap();
        println!("Remaining player points : {}", p.points.to_string());
    }
}
