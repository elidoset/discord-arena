use serde_derive::{Deserialize, Serialize};
use std::fs::File;
use std::io::Read;

#[derive(Serialize, Deserialize)]
#[serde(rename = "discord_api")]
pub struct DiscordApi {
    pub token: String,
}
#[derive(Serialize, Deserialize)]
pub struct Config {
    pub discord_api: DiscordApi,
}

pub fn init() -> Config {
    let mut file = File::open("Config.toml").expect("No config file found");
    let mut config_str = String::new();
    file.read_to_string(&mut config_str)
        .expect("Unable to read the file");
    toml::from_str(&config_str).expect("Could not deserialize parameters.")
}
