use serenity::{
    async_trait,
    http::Http,
    model::{channel::Channel, channel::Message, gateway::Ready},
    prelude::*,
    Client,
};

use serenity::framework::standard::{
    macros::*, Args, CommandGroup, CommandResult, StandardFramework,
};

mod config;

mod game;
mod general;
mod renderer;
mod utils;

use general::test::*;

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, msg: Message) {
        if msg.content == "$" {
            if let Err(why) = msg.channel_id.say(&ctx.http, "Pong!").await {
                println!("Error sending message: {:?}", why);
            }
        }
    }

    async fn ready(&self, _: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);
    }
}

#[group]
#[commands(test)]
struct General;

#[tokio::main]
async fn main() {
    let config = config::init();

    let mut game = game::Game::create();
    game.debug_init();
    //renderer::render_game().await;

    let framework = StandardFramework::new()
        .configure(|c| c.with_whitespace(true).prefix("!"))
        .group(&GENERAL_GROUP);

    let mut client = Client::builder(&config.discord_api.token)
        .event_handler(Handler)
        .framework(framework)
        .await
        .expect("Error creating the client");

    if let Err(err) = client.start().await {
        println!("Error ! {:?}", err);
    }
    println!("Hello, world!");
}
