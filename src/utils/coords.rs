pub struct Position {
    pub x: u32,
    pub y: u32,
}

impl Position {
    pub fn new(x: u32, y: u32) -> Position {
        Position { x: x, y: y }
    }
}

impl Clone for Position {
    fn clone(&self) -> Self {
        return Position {
            x: self.x,
            y: self.y,
        };
    }
}

impl Copy for Position {}

impl std::ops::Add for Position {
    type Output = Self;
    fn add(self, rhs: Self) -> Position {
        Position {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}
