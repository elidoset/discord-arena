use serenity::{
    framework::standard::{macros::*, Args, CommandResult},
    model::channel::Message,
    prelude::Context,
};

#[command]
pub async fn test(context: &Context, msg: &Message, _args: Args) -> CommandResult {
    msg.channel_id
        .send_files(context, vec!["image.jpg"], |m| m.content("NIK"))
        .await
        .expect("Could not send message");
    Ok(())
}
